package com.accenture.testing;

import static org.junit.Assert.assertEquals;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.Test;

import com.accenture.entity.Product;

public class SelectTest {
	
    

	@Test
	public void selectTest() {
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("unit1");
		
		EntityManager em = factory.createEntityManager();

		Product t = em.find(Product.class, 101);

		assertEquals("monitor",t.getpName());
		
	}
	
}
