package com.accenture.ui;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.accenture.entity.Product;

public class Product_Insert {

	public static void main(String[] args) {
		//load database config , open db
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("unit1");
	
		//rest of operation will be handle by EntityManager
		EntityManager em = factory.createEntityManager();
		
		em.getTransaction().begin();
		
		Product prod = new Product(101,"Computer", 35000,5);
		Product prod1 = new Product(102,"Computer", 35000,4);

		em.persist(prod); // converted to row insert query
		em.persist(prod1); // converted to row
		

		em.getTransaction().commit();
		em.close();
		factory.close();
	}

}
