package com.accenture.ui;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.accenture.entity.Product;

public class Product_Merge {

	public static void main(String[] args) {

		EntityManagerFactory factory = Persistence.createEntityManagerFactory("unit1");
		
		//rest of operation will be handle by EntityManager
		EntityManager em = factory.createEntityManager();
		
		em.getTransaction().begin();
		
		Product prod3 = new Product(103,"server",25000,3);//create new record as it is no available
		Product prod1 = new Product(101,"Printer",20000,3); // it will update record as it is already available
	
		em.merge(prod3);
		em.merge(prod1);
	
		em.getTransaction().commit();
		em.close();
		factory.close();
	}
}
