package com.accenture.ui;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.accenture.entity.Product;

public class Product_Select {

	public static void main(String[] args) {

		EntityManagerFactory factory = Persistence.createEntityManagerFactory("unit1");
		
		EntityManager em = factory.createEntityManager();
		
		
		Product temp = em.find(Product.class,102);
		//em.find(Product.class,102); 

		em.close();
		factory.close();
	}

}
