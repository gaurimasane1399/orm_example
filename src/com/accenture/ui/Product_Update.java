package com.accenture.ui;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.accenture.entity.Product;

public class Product_Update {

	public static void main(String[] args) {

		EntityManagerFactory factory = Persistence.createEntityManagerFactory("unit1");
		
		//rest of operation will be handle by EntityManager
		EntityManager em = factory.createEntityManager();
		
		em.getTransaction().begin();
		
		Product temp = em.find(Product.class, 101);
	
		if(temp!=null) {

			temp.setpName("Dell Laptop");
			temp.setPrice(40000);
	
		}
		else {
		System.out.println("Could not update .. as it's not available");
		}
	

		em.getTransaction().commit();
		em.close();
		factory.close();
	}

}
