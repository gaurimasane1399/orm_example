package com.accenture.ui;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.accenture.entity.Product;

public class Product_delete {

	public static void main(String[] args) {
		//load database config , open db
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("unit1");
	
		//rest of operation will be handle by EntityManager
		EntityManager em = factory.createEntityManager();
		
		em.getTransaction().begin();
		
		Product temp = em.find(Product.class, 102);
	
		if(temp!=null) {
			em.remove(102); // delete from product where id=104
			
		}
		else {
			System.out.println("Could not delete .. as it's not available");
		}
	

		em.getTransaction().commit();
		em.close();
		factory.close();
	}
}
